package sample;

import javafx.scene.text.Text;

import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

public class Table {
    // x contains -1 or an id of o piece
    static final int size = 8;
    static int[][] table = new int[size][size];
    Map<Integer,Piece> pieces = new HashMap<>();

    public Table(){
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                table[row][col] = -1;
            }
        }
        boolean k=true;
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < size; col++) {
                if(col==0) k = !k;
                if (k) {
                    int piece_id = AddPiece(row,col,3);
                    table[row][col]=piece_id;
                    k = false;
                } else {
                    k = true;
                }
            }
        }
        k=false;
        for (int row = 5; row < size; row++) {
            for (int col = 0; col < size; col++) {
                if(col==0) k = !k;
                if (k) {
                    int piece_id = AddPiece(row,col,1);
                    table[row][col]=piece_id;
                    k = false;
                } else {
                    k = true;
                }
            }
        }
    }
    public void Table_TEST(){
    for (int row = 0; row < size; row++) {
        for (int col = 0; col < size; col++) {
            table[row][col] = -1;
        }
    }

    int piece_id = AddPiece(1,1,3);
    table[1][1]=piece_id;
    piece_id = AddPiece(1,3,3);
    table[1][3]=piece_id;
    piece_id = AddPiece(3,1,3);
    table[3][1]=piece_id;

                piece_id = AddPiece(2,4,2);
                table[2][4]=piece_id;

}
    public boolean IsPiece(Point x){
        return table[x.x][x.y] != -1;
    }
    private boolean CheckValidNextPosition(Point point,int directionX,int directionY) {
        int new_X,new_Y;
        new_X=point.x+directionX;
        new_Y = point.y+directionY;
        if(CheckInBounds(new_X,new_Y)){
            return table[new_X][new_Y] == -1;
        }
        return  false;
    }
    private boolean CheckInBounds(int new_x, int new_y) {
        return new_x >= 0 && new_x < size && new_y >= 0 && new_y < size;
    }
    public boolean IsGameWon(Text x){
        int count_moves_user,count_moves_pc;
        List<Piece> pieces_user;
        pieces_user = pieces.values().stream().filter(Piece::GetPlayer).collect(Collectors.toList());
        List<Piece> pieces_computer;
        pieces_computer = pieces.values().stream().filter(e -> !e.GetPlayer()).collect(Collectors.toList());

        if(pieces_user.size()==0){
            x.setText("COMPUTER WON");
            return true;
        }
        if(pieces_computer.size()==0){
            x.setText("USER WON");
            return true;
        }
        count_moves_user = pieces_user.stream().mapToInt(e -> PossibleMoves(e).size()).sum();
        count_moves_pc = pieces_computer.stream().mapToInt(e -> PossibleMoves(e).size()).sum();
        if(count_moves_user==0) {
            x.setText("COMPUTER WON");
            return true;
        }
        else if(count_moves_pc==0) {
            x.setText("USER WON");
            return true;
        }
        return false;
    }
    public void MovePiece(Piece p,Point new_poz,LinkedList<Piece> pieces_to_capture){
        table[p.GetX()][p.GetY()]=-1;
        p.SetX(new_poz.x);
        p.SetY(new_poz.y);
        table[p.GetX()][p.GetY()]=p.GetPiece_id();
        if(pieces_to_capture != null)
        if(pieces_to_capture.size()>0){
            pieces_to_capture.forEach(x -> {
                table[x.GetX()][x.GetY()]=-1;
                pieces.remove(x.GetPiece_id());
            });
        }
        if(p.GetPlayer() && new_poz.x == 0){
            p.UpgradeToKing();
        }
        else if(!p.GetPlayer() && new_poz.x ==7){
            p.UpgradeToKing();
        }
    }
    public int AddPiece(int x, int y, int type){
        Piece p = new Piece(pieces.size(),x,y,type);
        pieces.put(p.GetPiece_id(),p);
        return p.GetPiece_id();
    }
    public LinkedList<Piece> GetComputerPieces(){
        LinkedList<Piece> comp_pieces = new LinkedList<>();
        for (Piece item : pieces.values()) {
            if(!item.GetPlayer())
                comp_pieces.add(item);
        }
        return  comp_pieces;
    }
    public Piece GetPieceById(int id){
        return pieces.get(id);
    }
    public Piece GetPieceByPoint(Point x){
        return GetPieceById(table[x.x][x.y]);
    }
    public void DisplayTable(){
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++){
                if(table[row][col] != -1)
                    System.out.print((GetPieceById(table[row][col]).GetType() )+" ");
                else
                    System.out.print(0+" ");
            }
            System.out.println();
        }
    }
    public Map<Point,LinkedList<Piece>> PossibleMoves(Piece p){
        int row = p.GetX();
        int col = p.GetY();
        int type = p.GetType();
        Map<Point,LinkedList<Piece>> moves= new HashMap<>();
        switch(type){
            case 1:
                TryMove(moves,new Point(row,col),new Point(row-1,col-1),p,new LinkedList<>());
                TryMove(moves,new Point(row,col),new Point(row-1,col+1),p,new LinkedList<>());
                break;
            case 2:
            case 4:
                TryMoveKing(moves,new Point(row,col),new Point(row-1,col-1),p,new LinkedList<>());
                TryMoveKing(moves,new Point(row,col),new Point(row-1,col+1),p,new LinkedList<>());
                TryMoveKing(moves,new Point(row,col),new Point(row+1,col-1),p,new LinkedList<>());
                TryMoveKing(moves,new Point(row,col),new Point(row+1,col+1),p,new LinkedList<>());
                break;
            case 3:
                TryMove(moves,new Point(row,col),new Point(row+1,col-1),p,new LinkedList<>());
                TryMove(moves,new Point(row,col),new Point(row+1,col+1),p,new LinkedList<>());
                break;
        }
        return moves;
    }
    private void TryMove(Map<Point,LinkedList<Piece>> moves,Point old_point,Point new_Point,Piece p,LinkedList<Piece> captures_pieces) {
        if(CheckInBounds(new_Point.x,new_Point.y)){
            if(table[new_Point.x][new_Point.y]==-1){
                // if there is no piece on the first new position
                if(old_point.x == p.GetX() && old_point.y == p.GetY()){
                    moves.put(new Point(new_Point.x,new_Point.y),captures_pieces);
                    return;
                }
                if(table[old_point.x][old_point.y]==-1){
                    return;
                }
                //check if player else computer
                if(p.GetPlayer()){
                    int count = moves.size();
                    TryMove(moves,new_Point,new Point(new_Point.x-1,new_Point.y-1),p,captures_pieces);
                    TryMove(moves,new_Point,new Point(new_Point.x-1,new_Point.y+1),p,captures_pieces);
                    if(moves.size() == count)
                        moves.put(new Point(new_Point.x,new_Point.y),captures_pieces);
                }
                else {
                    int count = moves.size();
                    TryMove(moves,new_Point,new Point(new_Point.x+1,new_Point.y-1),p,captures_pieces);
                    TryMove(moves,new_Point,new Point(new_Point.x+1,new_Point.y+1),p,captures_pieces);
                    if(moves.size() == count)
                        moves.put(new Point(new_Point.x,new_Point.y),captures_pieces);
                }
            }
            else {
                int directionX = new_Point.x-old_point.x;
                int directionY = new_Point.y-old_point.y;
                Piece new_piece = GetPieceById(table[new_Point.x][new_Point.y]);
                //if the old piece was -1(not a piece), we know that on the previous position it was not a piece of the opponent
                if(table[old_point.x][old_point.y] != -1){
                    Piece old_piece = GetPieceById(table[old_point.x][old_point.y]);
                    // if the new piece does not belong to the player (belongs to the opponent) and on the previous position it was not a piece of the opponent
                    if(p.GetPlayer() != new_piece.GetPlayer() && old_piece.GetPlayer() != new_piece.GetPlayer() && CheckValidNextPosition(new_Point,directionX,directionY)){
                        captures_pieces.add(new_piece);
                        TryMove(moves,new_Point,new Point(new_Point.x+directionX,new_Point.y+directionY),
                                p,captures_pieces);
                    }
                }
                else {
                    if(p.GetPlayer() != new_piece.GetPlayer() && CheckValidNextPosition(new_Point,directionX,directionY)){
                        captures_pieces.add(new_piece);
                        TryMove(moves,new_Point,new Point(new_Point.x+directionX,new_Point.y+directionY),
                                p,captures_pieces);
                    }
                }

            }
        }

    }
    private void TryMoveKing(Map<Point,LinkedList<Piece>> moves,Point old_point,Point new_Point,Piece p,LinkedList<Piece> captures_pieces){
        if(CheckInBounds(new_Point.x,new_Point.y)){
            if(table[new_Point.x][new_Point.y]==-1){
                // if there is no piece on the first new position
                if(old_point.x == p.GetX() && old_point.y == p.GetY()){
                    moves.put(new Point(new_Point.x,new_Point.y),captures_pieces);
                    return;
                }
                if(table[old_point.x][old_point.y]==-1){
                    return;
                }
                //doesn't matter if player or computer
                int direction_old_X = old_point.x-new_Point.x;
                int direction_old_Y = old_point.y-new_Point.y;

                int count = moves.size();
                if(direction_old_X!=-1 || direction_old_Y !=-1)
                    TryMoveKing(moves,new_Point,new Point(new_Point.x-1,new_Point.y-1),p,captures_pieces);
                if(direction_old_X!=-1 || direction_old_Y !=1)
                    TryMoveKing(moves,new_Point,new Point(new_Point.x-1,new_Point.y+1),p,captures_pieces);

                if(direction_old_X!=1 || direction_old_Y !=-1)
                    TryMoveKing(moves,new_Point,new Point(new_Point.x+1,new_Point.y-1),p,captures_pieces);
                if(direction_old_X!=1 || direction_old_Y !=1)
                    TryMoveKing(moves,new_Point,new Point(new_Point.x+1,new_Point.y+1),p,captures_pieces);
                if(moves.size() == count)
                    moves.put(new Point(new_Point.x,new_Point.y),captures_pieces);
            }
            else {
                int directionX = new_Point.x-old_point.x;
                int directionY = new_Point.y-old_point.y;
                Piece new_piece = GetPieceById(table[new_Point.x][new_Point.y]);
                //if the old piece was -1(not a piece), we know that on the previous position it was not a piece of the opponent
                if(table[old_point.x][old_point.y] != -1){
                    Piece old_piece = GetPieceById(table[old_point.x][old_point.y]);
                    // if the new piece does not belong to the player (belongs to the opponent) and on the previous position it was not a piece of the opponent
                    if(p.GetPlayer() != new_piece.GetPlayer() && old_piece.GetPlayer() != new_piece.GetPlayer() && CheckValidNextPosition(new_Point,directionX,directionY)){
                        captures_pieces.add(new_piece);
                        TryMoveKing(moves,new_Point,new Point(new_Point.x+directionX,new_Point.y+directionY),
                                p,captures_pieces);
                    }
                }
                else {
                    if(p.GetPlayer() != new_piece.GetPlayer() && CheckValidNextPosition(new_Point,directionX,directionY)){
                        captures_pieces.add(new_piece);
                        TryMoveKing(moves,new_Point,new Point(new_Point.x+directionX,new_Point.y+directionY),
                                p,captures_pieces);
                    }
                }

            }
        }
    }
}
