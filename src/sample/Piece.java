package sample;

import java.awt.*;

public class Piece {
    private final int piece_id;
    private int x;
    private int y;
    private int type;

    /**
     type:
     1,2 is the player, 3,4 is the computer
     2 and 4 are for double pieces
     */
    public Piece(int piece_id,int x,int y, int type){

        this.piece_id=piece_id;
        this.x=x;
        this.y=y;
        this.type = type;
    }
    /**return true for player, false for computer;*/
    public boolean GetPlayer(){
        return this.type==1 || this.type==2;
    }
    public int GetType(){
        return this.type;
    }
    public int GetPiece_id(){
        return piece_id;
    }
    public int GetX(){
        return x;
    }
    public int GetY(){
        return y;
    }
    public void SetX(int x){
        this.x=x;
    }
    public void SetY(int y){
        this.y=y;
    }
    public Point GetPoint(){
        return new Point(x,y);
    }
    public void UpgradeToKing(){
        type= (type==1 || type==2) ? 2 : 4;
    }
    public boolean IsKing(){
        return type != 2 && type != 4;
    }
}
