package sample;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.shape.*;
import javafx.scene.text.*;
import javafx.stage.Stage;
import java.awt.Point;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;

public class Main extends Application {
    static Text final_message = new Text();
    static final int size = 8;
    static Table table;
    static LinkedList<Piece> pieces_to_capture;

    public static void ComputerTurn(){
        Random rd = new Random();
        boolean at_least_one_move_chosen =false;
        LinkedList<Piece> pieces = table.GetComputerPieces();
        Map<Point,LinkedList<Piece>> moves;
        Piece p= pieces.get(0);
        Point pct = p.GetPoint();
        int taken_pieces = 0;
        for (Piece piece : pieces) {
            moves = table.PossibleMoves(piece);
            for (Point x : moves.keySet()) {
                if (moves.get(x).size()> taken_pieces) {
                    at_least_one_move_chosen = true;
                    taken_pieces = moves.get(x).size();
                    pct = x;
                    p = piece;
                    pieces_to_capture = moves.get(pct);
                } else if (moves.get(x).size() == taken_pieces){
                    boolean b = rd.nextBoolean();
                    if (b || !at_least_one_move_chosen) {
                        taken_pieces = moves.get(x).size();
                        pct = x;
                        p = piece;
                        pieces_to_capture = moves.get(pct);
                        at_least_one_move_chosen = true;
                    }
                }
            }
        }
        table.MovePiece(p,pct,pieces_to_capture);
        pieces_to_capture = null;
        //table.DisplayTable();
    }

    public static Node getNodeByRowColumnIndex (final int row, final int column, GridPane gridPane) {
        Node result = null;
        ObservableList<Node> children = gridPane.getChildren();

        for (Node node : children) {
            if(GridPane.getRowIndex(node) == row && GridPane.getColumnIndex(node) == column) {
                result = node;
                break;
            }
        }

        return result;
    }

    public static void ClearMoves(GridPane root) {
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++)
                if(!table.IsPiece(new Point(row,col))){
                    Node elem = getNodeByRowColumnIndex(row,col,root);
                    root.getChildren().remove(elem);
                    StackPane square = new StackPane();
                    String color;
                    if ((row + col) % 2 == 0) {
                        color = "white";
                    } else {
                        color = "black";
                    }
                    square.setStyle("-fx-background-color: " + color + ";");
                    root.add(square, col, row);
                }
        }
    }

    public static void DrawStage(Stage primaryStage) {
        //table.DisplayTable();
        GridPane root = new GridPane();
        // paint the game table
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                StackPane square = new StackPane();
                String color;
                if ((row + col) % 2 == 0) {
                    color = "white";
                } else {
                    color = "black";
                }
                square.setStyle("-fx-background-color: " + color + ";");
                root.add(square, col, row);
            }
        }
        //black pieces
        for (int row = 0; row < size; row++) {
            for (int col = 0; col < size; col++) {
                if(table.IsPiece(new Point(row,col)))
                    if (!table.GetPieceByPoint(new Point(row,col)).GetPlayer()) {
                        Circle r;
                        if(table.GetPieceByPoint(new Point(row, col)).IsKing()){
                             r = new Circle( 15,Paint.valueOf("#362c2c"));
                        }else {
                             r = new Circle( 15,Paint.valueOf("#660000"));
                        }
                        root.add(r, col, row);
                    }
            }
        }
        //player
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < size; col++)
                if(table.IsPiece(new Point(row,col))){
                if (table.GetPieceByPoint(new Point(row,col)).GetPlayer()) {
                    Circle r ;
                    if(table.GetPieceByPoint(new Point(row, col)).IsKing()){
                        r = new Circle(15, Paint.valueOf("white"));
                    } else {
                        r = new Circle(15, Paint.valueOf("yellow"));
                    }

                    int finalRow = row;
                    int finalCol = col;
                    r.setOnMouseClicked(e -> {
                        ClearMoves(root);
                        if(table.IsGameWon(final_message)) return;
                        Piece p = table.GetPieceByPoint(new Point(finalRow, finalCol));
                        Map<Point,LinkedList<Piece>> moves = table.PossibleMoves(p);
                        moves.forEach((x,z) ->{
                            Rectangle rr = new Rectangle(20, 20,Paint.valueOf("blue"));
                            rr.setOnMouseClicked(ev ->{
                                pieces_to_capture = moves.get(new Point(x.x,x.y));
                                table.MovePiece(p,new Point(x.x,x.y),pieces_to_capture);
                                pieces_to_capture = null;
                                if(table.IsGameWon(final_message)){
                                    DrawStage(primaryStage);
                                    return;
                                }
                                ComputerTurn();
                                table.IsGameWon(final_message);
                                DrawStage(primaryStage);
                            });
                            root.add(rr, x.y, x.x);
                        });
                    });
                    root.add(r, col, row);
                }
            }
        }
        for (int i = 0; i < size; i++) {
            root.getColumnConstraints().add(new ColumnConstraints(50, 50, Double.POSITIVE_INFINITY, Priority.ALWAYS, HPos.CENTER, true));
            root.getRowConstraints().add(new RowConstraints(50, 50, Double.POSITIVE_INFINITY, Priority.ALWAYS, VPos.CENTER, true));
        }
        Group group = new Group(root);
        if(table.IsGameWon(final_message)){
            root.setLayoutY(50);
            TextFlow textFlow = new TextFlow();
            textFlow.getChildren().add(final_message);
            textFlow.setPrefWidth(400);
            textFlow.setTextAlignment(TextAlignment.CENTER);

            group.getChildren().add(1,textFlow);
            primaryStage.setScene(new Scene(group, 400, 450));
        }
        else{
            primaryStage.setScene(new Scene(group, 400, 400));
        }
        primaryStage.show();
    }

    @Override
    public void start(Stage primaryStage) {
        table = new Table();
        final_message.setText("");
        final_message.setFont(new Font("Verdana",30));
        DrawStage(primaryStage);
    }
}